Feature: Search test

  @test
  Scenario: Test tag pollub
    Given I open pollub/dla-mediow
    When  I open "breadcrumbs-selected"
    Then Title of page must be "Dla Mediów"

  Scenario Outline: Test pollub
    Given I open pollub/dla-mediow
    When  I open "<page>"
    Then Title of page must be "<result>"

    Examples:
      | page            | result                |
      | extmenu-id_39   | Materiały do pobrania |
      | extmenu-id_1030 | Media o Politechnice  |
      | extmenu-id_169  | Rzecznik prasowy      |
      | extmenu-id_1061 | Archiwum wydarzeń     |

  @wip
  Scenario: Test a library on pollub
    Given I open pollub/biblioteka
    When  I open info about library
    Then Title of content must be "Informacje ogólne"