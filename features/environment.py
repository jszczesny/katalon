from behave import *
from selenium import webdriver


def before_scenario(context, scenario):
    context.driver = webdriver.Chrome()
    context.driver.set_window_size(1024, 600)
    context.driver.maximize_window()


def after_scenario(context, scenario):
    context.driver.close()
