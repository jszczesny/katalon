from behave import *
from selenium import webdriver


@given(u'I open pollub/dla-mediow')
def step_impl(context):
    context.driver.get("http://www.pollub.pl/")
    context.driver.find_element_by_id("accept-cookies-checkbox").click()
    context.driver.find_element_by_id("menu-id_5").click()


@when(u'I open "{page}"')
def step_impl(context, page):
    context.driver.find_element_by_id(page).click()


@then(u'Title of page must be "{result}"')
def step_impl(context, result):
    status = context.driver.find_element_by_xpath("//div[@id='mainContent']/h2").text
    assert status == result


@given(u'I open pollub/biblioteka')
def step_impl(context):
    context.driver.get("http://www.pollub.pl/")
    context.driver.find_element_by_id("accept-cookies-checkbox").click()
    context.driver.find_element_by_link_text("Biblioteka").click()


@when(u'I open info about library')
def step_impl(context):
    context.driver.find_element_by_xpath("//div[@id='mainpage_main']/div/div[2]/a/h2").click()


@then(u'Title of content must be "{result}"')
def step_impl(context, result):
    status = context.driver.find_element_by_xpath("//div[@id='content-inner']/h2").text

    assert status == result

